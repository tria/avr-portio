# portio by tria::avr

High level classes for GPIO control without performance or memory overhead, carefully crafted with some `asm` magic under the hood. No static things or templates (for the sake of easy passing as function arguments).

## Showcase

```cpp
#include <tria/avr/portio.h>

using namespace tria::avr;

// create port classes
portio pb(&PORTB, &DDRB, &PINB);

// set bit 7 as input and read it
pb.dir_clear_bit(7);
auto is_it_ok = pb.read_bit(7);

// or use temporary pin object with chain calls, still no overhead
is_it_ok = pin(pb, 7).input().read();

// or save the pin for later use
pin decider = pin(pb, 7);
decider.input();
is_it_ok = decider.read(); // or chain call it with input

// create a led and initialize it
pin led(pb, 0);
led.output().low();

// use it inside a function
void blink(pin led, uint16_t times) {
	for (auto i = 0; i < times, i++) {
		led.toggle();
		_delay_ms(250);
	}
}
```

## What's in the box?

### `portio`

```cpp
portio(&PORTx, &DDRx, &PINx)

write(val)
write_bit(num, val)
read()
read_bit(num)

set(mask)
clear(mask)

set_bit(num)
clear_bit(num)
toggle_bit(num)

dir_write(val)
dir_write_bit(num, val)
dir_read()
dir_read_bit(num)

dir_set(mask)
dir_clear(mask)

dir_set_bit(num)
dir_clear_bit(num)
```

### `pin`

```cpp
pin(portio &, num)

dir_set()
dir_clear()

set()
clear()
toggle()

write(val)
read()

// these methods support chain calling
pin & input()
pin & output()
pin & pullup()
pin & no_pullup()

pin & high()
pin & low()
```

## Tests?

Uhm, there is only a WIP for now. But soon™.