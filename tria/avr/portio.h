#ifndef __TRIA_AVR_PORTIO__INCL__
#define __TRIA_AVR_PORTIO__INCL__

namespace tria::avr {

class pin;
class portio {
public:
    const volatile uint8_t *port, *ddr, *pin;
    constexpr portio(volatile uint8_t * port_, volatile uint8_t * ddr_, volatile uint8_t * pin_) :
        port(port_), ddr(ddr_), pin(pin_) {}

    inline void write(const uint8_t val) {
        asm volatile ("sts %[port], %[val]."
            :
            : [port] "I" (_SFR_IO_ADDR(*port)),
              [val]  "r" (val)
        );
    }
    inline void write_bit(const uint8_t num, const uint8_t val) {
        val? set_bit(num) : clear_bit(num);
    }

    inline uint8_t read() const {
        uint8_t val;
        asm volatile ("lds %[out], %[pin]"
            : [out] "=r" (val)
            : [pin] "I" (_SFR_IO_ADDR(*pin))
        );
        return val;
    }
    inline uint8_t read_bit(const uint8_t num) const {
        uint8_t val = 0;
        asm volatile ("sbic %[pin], %[num] \n\t"
                      "mov %[out], 1       \n\t"
                      "nop"
            : [out] "=r" (val)
            : [pin] "I" (_SFR_IO_ADDR(*pin)),
              [num] "I" (num)
        );
        return val;
    }

    inline void set(uint8_t mask) const {
        asm volatile ("sbr %[port], %[mask]"
            :
            : [port] "I" (_SFR_IO_ADDR(*port)),
              [mask] "X" (mask)
        );
    }
    inline void clear(uint8_t mask) const {
        asm volatile ("cbr %[port], %[mask]"
            :
            : [port] "I" (_SFR_IO_ADDR(*port)),
              [mask] "r" (mask)
        );
    }

    inline void set_bit(uint8_t bit) {
        asm volatile ("sbi %[port], %[bit]"
            :
            : [port] "I" (_SFR_IO_ADDR(PORTB)),
              [bit ] "I" (bit)
        );
    }
    inline void clear_bit(uint8_t bit) const {
        asm volatile ("cbi %[port], %[bit]"
            :
            : [port] "I" (_SFR_IO_ADDR(*port)),
              [bit ] "I" (bit)
        );
    }
    inline void toggle_bit(uint8_t bit) const {
        asm volatile ("sbi %[pin], %[bit]"
            :
            : [pin]  "I" (_SFR_IO_ADDR(*pin)),
              [bit ] "I" (bit)
        );
    }

    inline void dir_write(const uint8_t val) {
        asm volatile ("sts %[ddr], %[val]"
            :
            : [ddr] "I" (_SFR_IO_ADDR(*ddr)),
              [val] "r" (val)
        );
    }
    inline void dir_write_bit(const uint8_t num, const uint8_t val) {
        val? dir_set_bit(num) : dir_clear_bit(num);
    }

    inline uint8_t dir_read() {
        uint8_t val;
        asm volatile ("lds %[out], %[ddr]"
            : [out] "=r" (val)
            : [ddr] "I"  (_SFR_IO_ADDR(*ddr))
        );
        return val;
    }
    inline uint8_t dir_read_bit(const uint8_t num) const {
        uint8_t val = 0;
        asm volatile ("sbic %[ddr], %[num] \n\t"
                      "mov %[out], 1       \n\t"
                      "nop"
            : [out] "=r" (val)
            : [ddr] "I"  (_SFR_IO_ADDR(*ddr)),
              [num] "I"  (num)
        );
        return val;
    }

    inline void dir_set(uint8_t mask) const {
        asm volatile ("sbr %[ddr], %[mask]"
            :
            : [ddr]  "I" (_SFR_IO_ADDR(*ddr)),
              [mask] "r" (mask)
        );
    }
    inline void dir_clear(uint8_t mask) const {
        asm volatile ("cbr %[ddr], %[mask]"
            :
            : [ddr]  "I" (_SFR_IO_ADDR(*ddr)),
              [mask] "r" (mask)
        );
    }

    inline void dir_set_bit(uint8_t bit) const {
        asm volatile ("sbi %[ddr], %[bit]"
            :
            : [ddr] "I" (_SFR_IO_ADDR(*ddr)),
              [bit] "I" (bit)
        );
    }
    inline void dir_clear_bit(uint8_t bit) const {
        asm volatile ("cbi %[ddr], %[bit]"
            :
            : [ddr] "I" (_SFR_IO_ADDR(*ddr)),
              [bit] "I" (bit)
        );
    }

};

class pin {
public:
    portio & port;
    const uint8_t num;

    constexpr pin(portio & port_, uint8_t num_) : port(port_), num(num_) {}

    inline void dir_set()   { port.dir_set_bit(num);   }
    inline void dir_clear() { port.dir_clear_bit(num); }

    inline void set()    { port.set_bit(num);   }
    inline void clear()  { port.clear_bit(num); }
    inline void toggle() { port.toggle_bit(num); }

    inline void write(const uint8_t val) {
     val? set() : clear();
    }
    inline uint8_t read() const {
        uint8_t val = port.read_bit(num);
        return val;
    }

    inline pin & input()     { dir_clear(); return *this; }
    inline pin & output()    { dir_set();   return *this; }
    inline pin & pullup()    { set();       return *this; }
    inline pin & no_pullup() { clear();     return *this; }

    inline pin & high() { set();   return *this; }
    inline pin & low()  { clear(); return *this; }
};

}

#endif